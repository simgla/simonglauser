$(document).foundation();

//fullpage
new fullpage('#fullpage', {
  //options 
  licenseKey: '317B759B-82B74BF6-9F316921-0B776375',
  anchors: ['home', 'work'],
  sectionsColor: ['none', 'black'],
  autoScrolling: true,
  scrollHorizontally: true,
  scrollingSpeed: 700,
  easingcss3: 'ease',
  controlArrows: false,
  parallax: false,
  parallaxOptions: {
    type: 'cover',
    percentage: 30,
    property: 'translate'
  },
  fixedElements: '.particles-js',

  onLeave: function (origin, destination, direction) {
    var leavingSection = this;

    //after leaving section actions
    if (origin.index == 0 && direction == 'down') {
      console.log('leaving home');
      $('.sticky-nav').addClass('active');
      $('#arrow-anchor').removeClass('arrow-down').addClass('arrow-up');
      $('.interaction-info').html('scroll up<br />to home');
    } else if (origin.index == 1 && direction == 'up') {
      console.log('leaving work');
      $('.sticky-nav').removeClass('active');
      $('#arrow-anchor').removeClass('arrow-up').addClass('arrow-down');
      $('.interaction-info').html('scroll down<br />to work');
    }
  }
});

//methods
fullpage_api.setAllowScrolling(true);

$(document).on('click', '.arrow-down', function () {
  fullpage_api.moveSectionDown();
});

$(document).on('click', '.arrow-up', function () {
  fullpage_api.moveSectionUp();
});

//parallax
var scene = document.getElementById('scene');
var parallaxInstance = new Parallax(scene);

//particles
particlesJS.load('particles-js', 'assets/particles.json', function () {
  console.log('callback - particles.js config loaded');
});

//scramble text
TweenLite.to('#anchor-about', 1, {
  scrambleText: "THIS IS NEW TEXT"
});


// custom cursor
document.addEventListener("DOMContentLoaded", function (event) {
  var cursor = document.querySelector(".custom-cursor");
  var links = document.querySelectorAll("a");
  var initCursor = false;

  for (var i = 0; i < links.length; i++) {
    var selfLink = links[i];

    selfLink.addEventListener("mouseover", function () {
      cursor.classList.add("custom-cursor--link");
    });
    selfLink.addEventListener("mouseout", function () {
      cursor.classList.remove("custom-cursor--link");
    });
  }

  window.onmousemove = function (e) {
    var mouseX = e.clientX;
    var mouseY = e.clientY;

    if (!initCursor) {
      // cursor.style.opacity = 1;
      TweenLite.to(cursor, 0.3, {
        opacity: 1
      });
      initCursor = true;
    }

    TweenLite.to(cursor, 0, {
      top: mouseY + "px",
      left: mouseX + "px"
    });
  };

  window.onmouseout = function (e) {
    TweenLite.to(cursor, 0.3, {
      opacity: 0
    });
    initCursor = false;
  };
});